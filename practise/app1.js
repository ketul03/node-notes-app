const yargs1 = require('yargs')
const notes1 = require('./notes1.js')

yargs1.command({
    command: 'add',
    describe: 'Add a note',
    builder: {
        title: {
            describe: 'Note title',
            demandOption: true,
            type: 'string'
        },
        body: {
            describe: 'Note body',
            demandOption: true,
            type: 'string'
        }
    },
    handler: function (argv) {
        notes1.addNotes(argv.title, argv.body)
    }
})

yargs1.command({
    command: 'remove',
    describe: 'Remove a note',
    builder: {
        title: {
            describe: 'Note Title',
            demandOption: true,
            type: 'string'
        }
    },
    handler: function (argv) {
        notes1.removeNotes(argv.title)
    }
})

yargs1.parse()