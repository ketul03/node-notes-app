With the help of notes-app you can perform below operations:

Add a note:
sample command: node app.js add --title="butter" --body="200mg butter pack"

Remove a note:
sample command: node app.js remove --title="butter"

View all notes:
sample command: node app.js list

Read a note:
sample command: node app.js read --title="butter"
