// standard function
// const square = function (x) {
//     return x * x
// }

// standard arrow function
// const square = (x) => {
//     return x * x
// }

//arrow function with short hand syntax
// const square = (x) => x * x

// console.log(square(4))

const partyEvent = {
    name: 'Birthday Party',
    guestList: ['Parth', 'Ankit', 'Deepak'],
    printGuestList() {
        console.log('Guest list for ' + this.name)
        this.guestList.forEach((guest) => console.log(guest + ' is attending ' + this.name))
        // here standard function won't work for "this" so we are using arrow function
    }
    // here we can't use arrow function because they don't bind their own "this" value
    // we can use ES6 function here
}

// key note: We will use arrow functions instead of standard functions
// If we come across a scenario with "this" binding then we will use ES6 functions

partyEvent.printGuestList()