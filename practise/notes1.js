const fs1 = require('fs')
const { title } = require('process')

const addNotes = function (title, body) {
    const fetchNotes = loadNotes()
    const dupliNotes = fetchNotes.filter(function (fetchNote) {
        return fetchNote.title === title
    })
    if (dupliNotes.length === 0) {
        fetchNotes.push({
            title: title,
            body: body
        })
        savenotes(fetchNotes)
        console.log('Note saved successfully.')
    } else {
        console.log('Note title exists.')
    }
}

const removeNotes = function (title) {
    const fetchNotes = loadNotes()
    const notesPresent = fetchNotes.filter(function (fetchNote) {
        return fetchNote.title === title
    })
    if (notesPresent.length === 0) {
        console.log('Note not exists.')
    }
    else {
        fetchNotes.splice({
            title: title
        })
        savenotes(fetchNotes)
        console.log('Note deleted.')
    }
}

const loadNotes = function () {
    try {
        const dataBuffer = fs1.readFileSync('notes1.json')
        const dataJSON = dataBuffer.toString()
        return JSON.parse(dataJSON)
    }
    catch (error) {
        return []
    }
}

const savenotes = function (notes) {
    const notesJSON = JSON.stringify(notes)
    fs1.writeFileSync('notes1.json', notesJSON)
}

module.exports = {
    addNotes: addNotes,
    removeNotes: removeNotes
}