const fs = require('fs')
const chalk = require('chalk')
const { title } = require('process')

const addNotes = (title, body) => {
    const notes = loadNotes()
    const duplicateNote = notes.find((note) => note.title === title)
    if (!duplicateNote) {
        notes.push({
            title: title,
            body: body
        })
        saveNotes(notes)
        console.log(chalk.green.inverse('Note added'))
    }
    else {
        console.log(chalk.red.inverse('Note exists'))
    }
}

const removeNotes = (title) => {
    const notes = loadNotes()
    const notesToKeep = notes.filter((note) => note.title !== title)
    if (notes.length > notesToKeep.length) {
        saveNotes(notesToKeep)
        console.log(chalk.green.inverse('Note deleted'))
    }
    else {
        console.log(chalk.red.inverse('Note does not exists'))
    }
}

const readNote = (title) => {
    const notes = loadNotes()
    const searchedNote = notes.find((note) => note.title === title)
    if (searchedNote) {
        console.log(chalk.inverse(searchedNote.title))
        console.log(searchedNote.body)
    }
    else {
        console.log(chalk.red.inverse('Note not found'))
    }
}

const listNotes = () => {
    const notes = loadNotes()
    console.log(chalk.inverse('Your Notes:'))
    notes.forEach((note) => console.log(note.title))
}

const saveNotes = (notes) => {
    const notesJSON = JSON.stringify(notes)
    fs.writeFileSync('notes.json', notesJSON)
}

const loadNotes = () => {
    try {
        const dataBuffer = fs.readFileSync('notes.json')
        const dataJSON = dataBuffer.toString()
        return JSON.parse(dataJSON)
    } catch (error) {
        return []
    }
}

module.exports = {
    addNotes: addNotes,
    removeNotes: removeNotes,
    listNotes: listNotes,
    readNote: readNote
}